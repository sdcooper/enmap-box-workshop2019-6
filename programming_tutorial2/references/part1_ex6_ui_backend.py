
from enmapbox.testing import initQgisApplication

# programming_tutorial2/references needs to be added to the PYTHONPATH
# In PyCharm Project View -> Mark Directory `programming_tutorial2/references` as `Sources Root`
from myworkshopapp.enmapboxworkshopui import EnMAPBoxWorkshopUI

app = initQgisApplication()

w = EnMAPBoxWorkshopUI()
w.show()

app.exec_()
