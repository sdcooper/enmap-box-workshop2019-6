from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *

from qgis.core import *
from qgis.gui import *

from enmapbox.testing import initQgisApplication

app = initQgisApplication()
from enmapbox.gui import SpectralProfile, SpectralLibrary, SpectralLibraryWidget, SpatialPoint, SpatialExtent
from enmapboxtestdata import enmap

# collect 10 SpectraProfiles from center of the EnMAP test image
# to the south in steps of 200 meters

enmapLayer = QgsRasterLayer(enmap)
center = SpatialPoint.fromMapLayerCenter(enmapLayer)

step = 200
profiles = []
for i in range(10):
    position = SpatialPoint(center.crs(), center.x(), center.y() - i * step)
    profile = SpectralProfile.fromRasterSource(enmapLayer, position)
    profile.setName('EnMAP Profile {}'.format(i + 1))
    profiles.append(profile)

speclib = SpectralLibrary()
speclib.startEditing()
speclib.addProfiles(profiles)
speclib.commitChanges()

print('First profile:'.format(speclib[0]))

print('Number of profiles: {}'.format(speclib))
for i, profile in enumerate(speclib):
    assert isinstance(profile, SpectralProfile)
    print('Profile {} "{}": {}'.format(i, profile.name(), profile.yValues()))

w = SpectralLibraryWidget(speclib=speclib)
w.setWindowTitle('SpectralLibraryWidget')
w.show()

canvas = QgsMapCanvas()
canvas.setWindowTitle('QgsMapCanvas')
QgsProject.instance().addMapLayers([enmapLayer, speclib])
canvas.setLayers([speclib, enmapLayer])
canvas.setDestinationCrs(enmapLayer.crs())
canvas.setExtent(SpatialExtent.fromLayer(speclib).toCrs(enmapLayer.crs()))
canvas.show()

app.exec_()
