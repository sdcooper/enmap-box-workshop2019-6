from enmapbox.testing import initQgisApplication
from qgis.PyQt.QtWidgets import QApplication
from qgis.core import QgsApplication, QgsRasterLayer, QgsCoordinateReferenceSystem

app = initQgisApplication()

assert isinstance(app, QApplication)
assert isinstance(app, QgsApplication)

uri = r'crs=EPSG:3857&format&type=xyz&url=https://mt1.google.com/vt/lyrs%3Ds%26x%3D%7Bx%7D%26y%3D%7By%7D%26z%3D%7Bz%7D&zmax=19&zmin=0'
layer = QgsRasterLayer(uri, 'google maps', 'wms')

# import enmapboxtestdata
# layer = QgsRasterLayer(enmapboxtestdata.enmap)

assert layer.isValid()

from qgis.gui import QgsMapCanvas

canvas = QgsMapCanvas()
canvas.setWindowTitle('Hello Real World')
canvas.setLayers([layer])
canvas.setExtent(layer.extent())
canvas.setDestinationCrs(layer.crs())

# canvas.setDestinationCrs(QgsCoordinateReferenceSystem('EPSG:4326'))
canvas.setDestinationCrs(QgsCoordinateReferenceSystem('EPSG:32632'))
canvas.show()

app.exec_()

print('Example 2 finished')