import os
from qgis.core import *
from qgis.gui import *
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *


pathUi = os.path.join(os.path.dirname(__file__), 'enmapboxworkshopui.ui')
from enmapbox.gui.utils import loadUIFormClass
class EnMAPBoxWorkshopUI(QWidget, loadUIFormClass(pathUi)):

    sigSpectralProfileRequest = pyqtSignal()

    def __init__(self, parent=None):
        super(EnMAPBoxWorkshopUI, self).__init__(parent)
        self.setupUi(self)

        assert isinstance(self.applicationLogo, QLabel)
        assert isinstance(self.btnSelectProfiles, QToolButton)
        assert isinstance(self.btnClear, QToolButton)
        assert isinstance(self.plotWidget, QGraphicsView)
        assert isinstance(self.btnBackgroundColor, QgsColorButton)

        #self.btnSelectProfiles.setIcon(QIcon('://enmapbox/gui/ui/icons/spectralprofile.svg'))
        #self.btnClear.setIcon(QIcon(':/images/themes/default/mIconClearText.svg'))


        from pyqtgraph import PlotWidget
        assert isinstance(self.plotWidget, PlotWidget)

        self.btnBackgroundColor.setColor(self.plotWidget.backgroundBrush().color())
        self.btnBackgroundColor.colorChanged.connect(self.plotWidget.setBackgroundBrush)

        self.btnSelectProfiles.clicked.connect(lambda: self.sigSpectralProfileRequest.emit())
        self.btnClear.clicked.connect(self.clearPlot)

    def addSpectralProfiles(self, spectralProfiles:list):
        """
        Plots spectral profiles
        :param spectralProfiles: list of spectral profiles
        """
        from enmapbox.gui import SpectralProfile
        from pyqtgraph import PlotItem, PlotDataItem
        assert isinstance(spectralProfiles, list)

        plotItem = self.plotWidget.getPlotItem()
        assert isinstance(plotItem, PlotItem)

        for spectralProfile in spectralProfiles:
            assert isinstance(spectralProfile, QgsFeature)
            spectralProfile = SpectralProfile.fromSpecLibFeature(spectralProfile)
            assert isinstance(spectralProfile, SpectralProfile)

            pdi = PlotDataItem(x=spectralProfile.xValues(), y=spectralProfile.yValues())
            plotItem.addItem(pdi)

    def clearPlot(self):
        """
        Removes all plot data items
        """
        self.plotWidget.plotItem.clear()